import React from 'react'
import { Create, SimpleForm, TextInput } from 'react-admin'

const ClientCreate = (props) => {
  return (
    <Create title='Create a Post' {...props}>
      <SimpleForm>
        <TextInput source='name' />
        <TextInput source='phone' />
        <TextInput source='email' />
      </SimpleForm>
    </Create>)
}

export default ClientCreate
