import React from 'react'
import { Edit, SimpleForm, TextInput } from 'react-admin'

const ClientEdit = (props) => {
  return (
    <Edit title='Edit a Post' {...props}>
      <SimpleForm>
        <TextInput source='id' disabled />
        <TextInput source='name' />
        <TextInput source='phone' />
        <TextInput source='email' />
      </SimpleForm>

    </Edit>)
}

export default ClientEdit
