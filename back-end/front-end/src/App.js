import React from 'react';
import { Admin, Resource } from 'react-admin';
import lb4Provider from 'react-admin-lb4';
import ClientCreate from './components/ClientCreate';
import ClientEdit from './components/ClientEdit';
import ClientList from './components/ClientList';

function App() {
  return (
    <Admin dataProvider={lb4Provider('http://localhost:5000')}>
      <Resource name='clients' list={ClientList} create={ClientCreate} edit={ClientEdit} />
    </Admin>
  );
}

export default App;
