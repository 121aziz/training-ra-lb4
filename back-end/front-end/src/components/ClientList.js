import React from 'react'
import { Datagrid, DeleteButton, EditButton, List, TextField } from 'react-admin'

const ClientList = (props) => {
    return (<List {...props}>
        <Datagrid>
            <TextField source='id' />
            <TextField source='name' />
            <TextField source='phone' />
            <TextField source='email' />
            <EditButton basePath='/clients' />
            <DeleteButton basePath='/clients' />
        </Datagrid>

    </List>)
}

export default ClientList
